import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { CustomerService } from '../service/customer.service';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css']
})
export class AddCustomerComponent implements OnInit {

  addForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, private customerService: CustomerService) { }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      id:[{value: '', disabled: true}],
      name:['',Validators.required],
      code:['',Validators.required],
      address:['',Validators.required],
      enabled:[false,Validators.required]
    });
  }

  onSubmit() {
    console.log(this.addForm.value);
    if (this.addForm.invalid) {
      return;
    }
    this.customerService.createCustomer(this.addForm.value)    
    .subscribe(
      data=> {
        this.router.navigate(['list-customer']);
      }
    );
  }
}