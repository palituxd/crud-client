import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

//import { AppRoutingModule } from './app-routing.module';
import { AppRoutingModule } from "./app-crud-routing.module";
//import { AppComponent } from './app.component';
import { LoginComponent } from "./login/login.component";
import { AddCustomerComponent } from "./add-customer/add-customer.component";
import { EditCustomerComponent } from "./edit-customer/edit-customer.component";
import { ListCustomerComponent } from "./list-customer/list-customer.component";
import { AppComponent } from "./app-crud.component";
import { AuthenticationService } from "./service/auth.service";
import { CustomerService } from "./service/customer.service";
import { PasswordValidator } from "./login/password-validator";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatButtonModule } from "@angular/material";
import { MatInputModule } from "@angular/material";
import { MatFormFieldModule } from "@angular/material";
import { MatIconModule } from "@angular/material";
import { MatTableModule } from "@angular/material";
import { MatDialogModule } from "@angular/material";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AddCustomerComponent,
    EditCustomerComponent,
    ListCustomerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatTableModule,
    MatDialogModule
  ],
  providers: [AuthenticationService, CustomerService, PasswordValidator],
  bootstrap: [AppComponent]
})
export class AppModule {}
