import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Customer } from '../model/customer.model';
import { Router } from '@angular/router';
import { CustomerService } from '../service/customer.service';
import { first } from "rxjs/operators";

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.css']
})
export class EditCustomerComponent implements OnInit {

  editForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, private customerService: CustomerService) { }

  ngOnInit() {
    let userId = localStorage.getItem("editCustomerId");
    if(!userId) {
      alert("Invalid action.")
      this.router.navigate(['list-customer']);
      return;
    }
    
    this.editForm = this.formBuilder.group({
      id:[{value: '', disabled:true}],
      name:['',Validators.required],
      code:['',Validators.required],
      address:['',Validators.required],
      enabled:[false,Validators.required]
    });
    this.customerService.getCustomer(+userId).subscribe(data=>{
      this.editForm.setValue(data);
    });
    console.log(userId);
  }

  onSubmit() {
    console.log(this.editForm.getRawValue());
    this.customerService.updateCustomer(this.editForm.getRawValue())
    .pipe(first())
    .subscribe(
      data=>{ 
        console.log(data); 
        this.router.navigate(['list-customer']) 
      },
      error=> { 
        console.log(error); 
        alert(error.headers); 
      });
  }
}
