import { Injectable } from '@angular/core';

@Injectable()
export class AuthenticationServiceMock {

  constructor() { }

  login(username:string, password:string) {
    return true;
  }
}