export class Customer {
    id: number;
    name: string;
    address: string;
    code: string;
    enabled: boolean;
}
