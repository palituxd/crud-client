const EXPECTED_CHARS = ['@', '$', '&']
export class PasswordValidator {
    validate(password: String) {
        let valido: Boolean;
        valido = false;
        EXPECTED_CHARS.forEach((char) => {
            if (password.indexOf(char) >= 0) {
                valido = true;
            }
        })
        return valido;
    }
}