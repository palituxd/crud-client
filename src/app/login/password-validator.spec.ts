import { async } from "@angular/core/testing";
import { PasswordValidator } from "./password-validator";

describe("test PasswordValidator", () => {
  it("password invalido", async(() => {
    let validator: PasswordValidator;
    validator = new PasswordValidator();
    expect(validator.validate("12345")).toBeFalsy();
  }));

  it("password valido", async(() => {
    let validator: PasswordValidator;
    validator = new PasswordValidator();
    expect(validator.validate("12&45")).toBeTruthy();
  }));

  it("password valido 2", async(() => {
    let validator: PasswordValidator;
    validator = new PasswordValidator();
    expect(validator.validate("@2&45")).toBeTruthy();
  }));
});
