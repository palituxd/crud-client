// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts
const { configure } = require("log4js");
const { SpecReporter } = require("jasmine-spec-reporter");
const ALLURE_PATH = "reports/allure-results";
var AllureReporter = require("jasmine-allure-reporter");
var Runtime = require("allure-js-commons/runtime");
var Jasmine2AllureReporter = require("jasmine-allure-reporter/src/Jasmine2AllureReporter.js");
var allure = new Runtime(Jasmine2AllureReporter.allureReporter);

exports.config = {
  allScriptsTimeout: 11000,
  specs: ["./src/**/*.e2e-spec.ts"],
  capabilities: {
    browserName: "chrome"
  },
  directConnect: true,
  baseUrl: "http://localhost:4200/",
  framework: "jasmine",
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  onPrepare() {
    configure(require("path").join(__dirname, "./log4js.json"));
    jasmine.getEnv().addReporter(
      new AllureReporter({
        resultsDir: ALLURE_PATH
      })
    );
    require("ts-node").register({
      project: require("path").join(__dirname, "./tsconfig.e2e.json")
    });
    jasmine
      .getEnv()
      .addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
    jasmine.getEnv().afterEach(function(done) {
      browser.takeScreenshot().then(function(png) {
        allure.createAttachment(
          "Screenshot",
          function() {
            return new Buffer(png, "base64");
          },
          "image/png"
        )();
        done();
      });
    });
  }
};
