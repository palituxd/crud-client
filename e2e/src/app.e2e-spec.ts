import { AppPage } from "./app.po";
import { browser, logging } from "protractor";

describe("workspace-project App", () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    page.navigateTo();
  });

  it("should display welcome message", () => {
    expect(page.getTitleText()).toEqual("Login");
    page.putUsername("pablo@pablo.org1");
    page.putPassword("pablo$1");
    page.doClickOnLogin();
    browser.sleep(20000);
    let count = page.customerCount();
    console.log(count);
    page.doClickOnAddUser();
    browser.sleep(30000);
    page.putNewUsername("new_user1");
    page.putNewCode("new_code1");
    page.putNewAddress("new_address1");
    page.doClickOnAddButton();
    browser.sleep(30000);
    expect(page.customerCount()).toBeGreaterThan(count);
    browser.sleep(30000);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser
      .manage()
      .logs()
      .get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE
      } as logging.Entry)
    );
  });
});
