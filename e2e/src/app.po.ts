import { browser, by, element } from "protractor";
import { Action, SelectorType } from "./shared/common/action";

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getTitleText() {
    return element(by.css("h1")).getText() as Promise<string>;
  }

  async putUsername(username) {
    let xpath = "email";
    await Action.set({
      selector: xpath,
      type: SelectorType.ID
    }).putText(username);
  }

  async putNewUsername(username) {
    let xpath = "//input[@name='name']";
    await Action.set({
      selector: xpath,
      type: SelectorType.XPATH
    }).putText(username);
  }

  async putNewCode(code) {
    let xpath = "//input[@name='code']";
    await Action.set({
      selector: xpath,
      type: SelectorType.XPATH
    }).putText(code);
  }

  async putNewAddress(address) {
    let xpath = "//input[@name='address']";
    await Action.set({
      selector: xpath,
      type: SelectorType.XPATH
    }).putText(address);
  }

  async doClickOnAddButton() {
    let xpath = "button";
    await Action.set({
      selector: xpath,
      type: SelectorType.CSS
    }).doClick();
  }

  async putPassword(password) {
    let xpath = "password";
    await Action.set({
      selector: xpath,
      type: SelectorType.ID
    }).putText(password);
  }

  async doClickOnLogin() {
    let xpath = "button";
    await Action.set({
      selector: xpath,
      type: SelectorType.CSS
    }).doClick();
  }

  async doClickOnAddUser() {
    let xpath = "button";
    await Action.set({
      selector: xpath,
      type: SelectorType.CSS
    }).doClick();
  }

  async customerCount() {
    let list = element.all(by.xpath("//button[@color='accent']"));
    return list.count();
  }
}
