import { browser, element, by, protractor } from "protractor";
import { getLogger } from "log4js";
const logger = getLogger("Action");

export enum SelectorType {
  XPATH,
  ID,
  CSS
}

export interface Selector {
  selector: any;
  type: SelectorType;
  coincidence?: number;
}

export class Action {
  private constructor(private selector: Selector) {
    if (
      this.selector.coincidence === null ||
      this.selector.coincidence === undefined
    ) {
      this.selector.coincidence = 0;
    }
  }

  public static set(selector: Selector): Action {
    return new Action(selector);
  }

  public static async goTo(url) {
    await browser.get(url);
  }

  private getComponent() {
    let component = null;
    switch (this.selector.type) {
      case SelectorType.XPATH:
        component = element
          .all(by.xpath(this.selector.selector))
          .get(this.selector.coincidence);
        break;
      case SelectorType.ID:
        component = element
          .all(by.id(this.selector.selector))
          .get(this.selector.coincidence);
        break;
      case SelectorType.CSS:
        component = element
          .all(by.css(this.selector.selector))
          .get(this.selector.coincidence);
        break;
    }
    return component;
  }

  public toString(): String {
    return (
      "{selector:" +
      this.selector.selector +
      ",type:" +
      this.selector.type +
      ",coincidence:" +
      this.selector.coincidence +
      "}"
    );
  }

  public async waitFor() {
    let component = this.getComponent();
    await browser.wait(
      protractor.ExpectedConditions.elementToBeClickable(component)
    );
  }

  public async waitForText(text) {
    let component = this.getComponent();
    await browser.wait(
      protractor.ExpectedConditions.textToBePresentInElement(component, text)
    );
  }

  public async doClick() {
    let component = this.getComponent();
    await browser
      .wait(protractor.ExpectedConditions.elementToBeClickable(component))
      .then(() => {
        component.click();
        logger.debug("Click:" + this.toString());
      });
  }

  public async putText(text: any) {
    let input = this.getComponent();
    await browser
      .wait(protractor.ExpectedConditions.visibilityOf(input))
      .then(() => {
        input.sendKeys(text);
        logger.debug("PutText:" + this.toString());
      });
  }

  public async getText() {
    let input = this.getComponent();
    let content = null;
    await browser
      .wait(protractor.ExpectedConditions.visibilityOf(input))
      .then(() => {
        logger.debug("GetText:" + this.toString());
        input.getText().then(text => {
          content = text;
          logger.debug("GetText->Result:" + text);
        });
      });
    return Promise.resolve(content);
  }
}
